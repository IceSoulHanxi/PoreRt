package blue.lapis.pore.mixin.net.minecraft.server;

import com.mojang.authlib.GameProfileRepository;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.management.PlayerProfileCache;
import net.minecraft.util.datafix.DataFixer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.io.File;
import java.net.Proxy;

@Mixin(MinecraftServer.class)
public abstract class MixinMinecraftServer {//fix protocollib ProtocolInjector fuzzyServer.getSingleton();
    @Shadow public abstract MinecraftServer getServer();
    private static MinecraftServer instance;
    @Inject(method = "<init>", at = @At("RETURN"))
    private void onInit(File anvilFileIn, Proxy proxyIn, DataFixer dataFixerIn,
                        YggdrasilAuthenticationService authServiceIn, MinecraftSessionService sessionServiceIn,
                        GameProfileRepository profileRepoIn, PlayerProfileCache profileCacheIn, CallbackInfo ci) {
        instance = this.getServer();
    }

    public static MinecraftServer getInstance(){
        return instance;
    }
}
