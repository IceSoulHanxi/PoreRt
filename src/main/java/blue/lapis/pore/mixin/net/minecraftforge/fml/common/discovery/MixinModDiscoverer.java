package blue.lapis.pore.mixin.net.minecraftforge.fml.common.discovery;

import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.common.discovery.ModCandidate;
import net.minecraftforge.fml.common.discovery.ModDiscoverer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Mixin(value = { ModDiscoverer.class }, remap = false)
public abstract class MixinModDiscoverer
{
    @Shadow private List<ModCandidate> candidates;

    @Overwrite
    public void addCandidate(final ModCandidate candidate) {
        for (final ModCandidate c : this.candidates) {
            if (this.equals(c.getModContainer(), candidate.getModContainer())) {
                FMLLog.log.trace("  Skipping already in list {}", (Object)candidate.getModContainer());
                return;
            }
        }
        this.candidates.add(candidate);
    }

    private boolean equals(final File f1, final File f2) {
        try {
            return f1.getCanonicalPath().equals(f2.getCanonicalPath());
        }
        catch (IOException e) {
            e.printStackTrace();
            return f1.getAbsolutePath().equals(f2.getAbsolutePath());
        }
    }
}