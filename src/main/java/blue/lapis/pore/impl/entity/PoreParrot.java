package blue.lapis.pore.impl.entity;

import blue.lapis.pore.converter.wrapper.WrapperConverter;
import org.bukkit.entity.AnimalTamer;
import org.spongepowered.api.entity.living.animal.Animal;
import org.spongepowered.api.entity.living.animal.Parrot;

public class PoreParrot extends PoreAnimals implements org.bukkit.entity.Parrot {

    public static PoreSheep of(Parrot handle) {
        return WrapperConverter.of(PoreParrot.class, handle);
    }

    protected PoreParrot(Animal handle) {
        super(handle);
    }

    @Override
    public Parrot getHandle() {
        return (Parrot) super.getHandle();
    }

    @Override
    public Variant getVariant() {
        return null;
    }

    @Override
    public void setVariant(Variant variant) {

    }

    @Override
    public boolean isSitting() {
        return false;
    }

    @Override
    public void setSitting(boolean sitting) {

    }

    @Override
    public boolean isTamed() {
        return false;
    }

    @Override
    public void setTamed(boolean tame) {

    }

    @Override
    public AnimalTamer getOwner() {
        return null;
    }

    @Override
    public void setOwner(AnimalTamer tamer) {

    }
}
